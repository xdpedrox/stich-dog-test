import logo from "./logo.svg";
import "./App.css";
import DogSort from "./components/DogSort";

function App() {
  return (
    <div className="App">
      <link
        async
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/semantic-ui@2/dist/semantic.min.css"
      />
      <script src="https://cdn.jsdelivr.net/npm/semantic-ui-react/dist/umd/semantic-ui-react.min.js"></script>

      <script src="like_button.js"></script>
      <DogSort />
    </div>
  );
}

export default App;
