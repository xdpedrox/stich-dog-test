import useDogSorter from "./useDogSorter";
import {
  Image,
  Table,
  Header,
  Divider,
  Select,
  Container,
  Icon,
} from "semantic-ui-react";
import classes from "./DogSort.module.css";
import moment from "moment";
function DogSort() {
  const [state, sortingOptions, toggleFavorite] = useDogSorter();

  return (
    <div>
      <Header as="h1">Dog Sorter</Header>
      <Select placeholder="Sort Dogs" options={sortingOptions} />
      <Divider />
      <div />
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Photo</Table.HeaderCell>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Breed</Table.HeaderCell>
            <Table.HeaderCell>Colour</Table.HeaderCell>
            <Table.HeaderCell>Age</Table.HeaderCell>
            <Table.HeaderCell>Value</Table.HeaderCell>
            <Table.HeaderCell>Favorite</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {state.data.map((dog) => (
            <Table.Row>
              <Table.Cell>
                <Image src={dog.photo} size="small" />
              </Table.Cell>
              <Table.Cell>{dog.name}</Table.Cell>
              <Table.Cell>{dog.breed}</Table.Cell>

              <Table.Cell>
                <div
                  className={classes.divColor}
                  {...(dog.colour.length === 2
                    ? {
                        style: {
                          background: `linear-gradient(to bottom right, ${dog.colour[0]} 50%,${dog.colour[1]} 50%)`,
                        },
                      }
                    : { style: { backgroundColor: dog.colour } })}
                />
              </Table.Cell>

              <Table.Cell> {moment().diff(dog.dateOfBirth, 'years',false)} Years</Table.Cell>
              <Table.Cell>{dog.value}</Table.Cell>
              <Table.Cell>
                {(dog.favorite && (
                  <Icon
                    name="star"
                    onClick={() => toggleFavorite(dog.id)}
                    color="yellow"
                  />
                )) || (
                  <Icon
                    name="star outline"
                    onClick={() => toggleFavorite(dog.id)}
                    color="yellow"
                  />
                )}
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>

      <Container text>
        <Header as="h2">Dog Stats</Header>
        <Header as="h3">{state.stats.total} Total Dogs</Header>
        <Header as="h3">{state.stats.breeds} Total Breeds</Header>
        <Header as="h3">
          £ {state.stats.averagePrice} Total Average Value
        </Header>
        <Header as="h3">{state.stats.favorites} Favorites</Header>
      </Container>
    </div>
  );
}

export default DogSort;
