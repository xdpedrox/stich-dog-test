import rawData from "../../data/data.js";
import { useEffect, useReducer } from "react";

function reducer(state, action) {
  switch (action.type) {
    case "SET_DATA":
      const breeds = [...new Set(action.data.map((dog) => dog.breed))];
      let totalValue = 0;

      action.data.forEach((dog) => {
        totalValue += dog.value;
      });

      return {
        data: action.data.map((dog) => {
          return {
            ...dog,
            favorite: false,
          };
        }),
        stats: {
          total: action.data.length,
          breeds: breeds.length,
          averagePrice: totalValue / action.data.length,
          favorites: 0,
        },
      };
    case "TOGGLE_FAVORITE":
      return {
        data: action.data,
        stats: {
          ...state.stats,
          // If we add a dog to favorites, we add 1 to the favorites count
          favorites: state.stats.favorites + (action.add ? 1 : -1),
        },
      };
    default:
      throw new Error();
  }
}

const useDogSorter = () => {
  const initialState = {
    data: [],
    stats: {
      total: 0,
      breeds: 0,
      averagePrice: 0,
      favorites: 0,
    },
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  const sortingOptions = [
    { key: "dog-name", value: "af", text: "By name" },
    { key: "dog-breed", value: "ax", text: "By breed" },
    { key: "dog-age", value: "al", text: "By age" },
    { key: "dog-value", value: "dz", text: "By value" },
  ];

  const toggleFavorite = (id) => {
    const newDogs = [...state.data];
    const index = newDogs.findIndex((item) => item.id === id);
    const add = state.data[index].favorite ? false : true;
    newDogs[index].favorite = !newDogs[index].favorite;

    dispatch({ type: "TOGGLE_FAVORITE", data: newDogs, add });
  };

  useEffect(() => {
    dispatch({ type: "SET_DATA", data: rawData });
  }, []);

  return [state, sortingOptions, toggleFavorite];
};

export default useDogSorter;
