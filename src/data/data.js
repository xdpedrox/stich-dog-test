const data = [
  {
    id: "1",
    name: "Russell",
    breed: "Jack Russell",
    dateOfBirth: "2008-07-14",
    value: 200,
    colour: ["Seashell"],
    photo:
      "https://res.cloudinary.com/stitch-group/image/upload/v1559561175/dogs/russell.png",
  },
  {
    id: "2",
    name: "Nathan",
    breed: "Labrador",
    dateOfBirth: "2014-06-10",
    value: 120,
    colour: ["BurlyWood"],
    photo:
      "https://res.cloudinary.com/stitch-group/image/upload/v1559561851/dogs/nathan.png",
  },
  {
    id: "3",
    name: "Rover",
    breed: "Labrador",
    dateOfBirth: "2016-08-25",
    value: 220,
    colour: ["SaddleBrown"],
    photo:
      "https://res.cloudinary.com/stitch-group/image/upload/v1559561850/dogs/rover.png",
  },
  {
    id: "4",
    name: "George",
    breed: "Dachshund",
    dateOfBirth: "2018-11-14",
    value: 320,
    colour: ["Black", "Sienna"],
    photo:
      "https://res.cloudinary.com/stitch-group/image/upload/v1559561852/dogs/george.png",
  },
  {
    id: "5",
    name: "Walt",
    breed: "Greyhound",
    dateOfBirth: "2014-07-14",
    value: 70,
    colour: ["Silver", "Snow"],
    photo:
      "https://res.cloudinary.com/stitch-group/image/upload/v1559561852/dogs/walt.png",
  },
];

export default data;
